describe('Credentials', function () {
  it('Creates a new credential', function () {
    cy.registerAndLogin('create@vault.com', 'password');

    cy.contains('Showing 0 credentials');
  });
});
