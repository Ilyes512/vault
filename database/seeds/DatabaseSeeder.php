<?php

declare(strict_types=1);

use App\Credential;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->create([
            'email' => 'bobby@vault.com',
            'password' => Hash::make('password'),
        ]);

        factory(Credential::class)->times(10)->create([
            'user_id' => $user->id,
        ]);
    }
}
