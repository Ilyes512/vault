<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');
Route::post('auth/logout', 'AuthController@logout');

Route::middleware(['auth:api'])->group(function () {
    Route::get('credentials', 'CredentialsController@index');
    Route::post('credentials', 'CredentialsController@store');
    Route::patch('credentials/{credential}', 'CredentialsController@update');
    Route::delete('credentials/{credential}', 'CredentialsController@destroy');
    Route::get('credentials/{credential}/password', 'PasswordsController@show');
});
