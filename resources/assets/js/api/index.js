/* global config */

import axios from 'axios';
import store from './../store';
import routes from './../routes';

const api = axios.create({
  baseURL: `${config.host}`,
  timeout: 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json'
  },
  responseType: 'json'
});

api.interceptors.request.use((config) => {
  config.headers.Authorization = 'Bearer ' + localStorage.getItem('token');

  return config;
}, error => Promise.reject(error));

api.interceptors.response.use(response => response, (error) => {
  const statusCode = error.response.status;

  if (statusCode === 401) {
    store.commit('logoutUser');

    localStorage.removeItem('token');

    routes.push({ name: 'login' });

    return Promise.reject(error);
  }

  return Promise.reject(error);
});

export default api;
