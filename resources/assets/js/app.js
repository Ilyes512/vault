import Vue from 'vue';
import routes from './routes';
import store from './store';
import Application from './Application.vue';
import VueSweetalert2 from 'vue-sweetalert2';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faPlusCircle, faSpinner, faTimesCircle } from '@fortawesome/free-solid-svg-icons';

library.add(faPlusCircle, faSpinner, faTimesCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueSweetalert2);

Vue.prototype.$vueEventBus = new Vue();

window.vue = new Vue({
  el: '#app',
  store: store,
  router: routes,
  template: '<Application/>',
  components: { Application }
});
