import Vue from 'vue';
import VueRouter from 'vue-router';

import store from './../store';

import DashboardPage from './../pages/DashboardPage';
import RegisterPage from './../pages/RegisterPage';
import LoginPage from './../pages/LoginPage';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: DashboardPage,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'login',
    component: LoginPage
  },
  {
    path: '/register',
    name: 'register',
    component: RegisterPage
  }
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
    next({ name: 'login' });
    return;
  }

  if (to.path === '/login' && store.state.isLoggedIn) {
    next({ name: 'dashboard' });
    return;
  }

  next();
});

export default router;
