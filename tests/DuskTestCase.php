<?php

declare(strict_types=1);

namespace Tests;

use App\Credential;
use App\User;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Collection;
use Laravel\Dusk\TestCase as BaseTestCase;

abstract class DuskTestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    /**
     * @var User
     */
    protected $user;

    /**
     * Prepare for Dusk test execution.
     *
     * @beforeClass
     * @return void
     */
    public static function prepare()
    {
        static::startChromeDriver();
    }

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass();

        parent::closeAll();
    }

    public function createDefaultCredentials(): Collection
    {
        $collection = collect();

        $credentialOverrides = [
            ['name' => 'Amazon'],
            ['name' => 'Zalando'],
            ['url' => 'https://amazon.com'],
            ['url' => 'https://google.com'],
            ['username' => 'superman-1234'],
            ['username' => 'superman-4321'],
        ];

        foreach ($credentialOverrides as $credentialOverride) {
            $collection->push($this->createCredential($credentialOverride));
        }

        return $collection;
    }

    public function createCredential(array $overrides = [], User $user = null): Credential
    {
        if (!$user instanceof User) {
            $user = $this->user;
        }

        $overrides['user_id'] = $user->id;

        return factory(Credential::class)->create($overrides);
    }

    /**
     * Create the RemoteWebDriver instance.
     *
     * @return \Facebook\WebDriver\Remote\RemoteWebDriver
     */
    protected function driver()
    {
        $options = (new ChromeOptions())->addArguments([
            '--disable-gpu',
             '--headless', // Must be used in the container, can be disabled locally to see the browser popup
            '--no-sandbox',
        ]);

        return RemoteWebDriver::create(
            'http://localhost:9515',
            DesiredCapabilities::chrome()->setCapability(
            ChromeOptions::CAPABILITY,
                $options
        )
        );
    }
}
