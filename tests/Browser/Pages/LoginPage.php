<?php

declare(strict_types=1);

namespace Tests\Browser\Pages;

use App\User;
use Laravel\Dusk\Browser;

class LoginPage extends Page
{
    /**
     * Get the URL for the page.
     *
     * @return string
     */
    public function url()
    {
        return '/login';
    }

    /**
     * Submit the form with the given credentials.
     *
     * @param Browser $browser
     * @param string $email
     * @param string $password
     */
    public function submit(Browser $browser, string $email, string $password)
    {
        $browser->assertSee('Login');
        $browser->type('email', $email);
        $browser->type('password', $password);
        $browser->press('Login');
        $browser->pause(1000);
    }

    /**
     * @param Browser $browser
     * @param User $user
     */
    public function loginAsUser(Browser $browser, User $user)
    {
        $browser->assertSee('Login');
        $browser->type('email', $user->email);
        $browser->type('password', 'secret');
        $browser->press('Login');
        $browser->pause(1000);
    }
}
