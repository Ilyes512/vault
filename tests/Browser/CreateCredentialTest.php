<?php

declare(strict_types=1);

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;

class CreateCredentialTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->createDefaultCredentials();
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function testCreatingNewCredential(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->assertSee('Showing 6 credentials')
                ->press('Credential')
                ->waitFor('.modal.show', 1)
                ->type('name', 'MyCredential')
                ->type('url', 'https://credential.com')
                ->type('username', 'MyUsername')
                ->type('password', 'MyPassword')
                ->press('Save credential')
                ->waitUntilMissing('.modal.show')
                ->assertSee('Showing 7 credentials');
        });
    }
}
