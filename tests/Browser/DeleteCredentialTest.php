<?php

declare(strict_types=1);

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\Browser\Pages\CredentialsPage;
use Tests\Browser\Pages\LoginPage;
use Tests\DuskTestCase;

class DeleteCredentialTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function setUp()
    {
        parent::setUp();

        $this->createCredential([
            'name' => 'DeletCredential',
            'url' => 'https://credential.com/delete',
            'username' => 'delete',
            'password' => encrypt('delete'),
        ]);
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function testDeleteCredential(): void
    {
        $this->browse(function (Browser $browser) {
            /** Browser $browser */
            $browser->visit(new LoginPage())->loginAsUser($this->user);

            $browser->visit(new CredentialsPage())
                ->waitUntilMissing('.credentials-loader')
                ->click('.card-link:nth-child(2)')
                ->waitFor('.swal2-shown')
                ->press('Yes, Delete credential!')
                ->press('OK')
                ->waitUntilMissing('.swal2-shown')
                ->assertSee('Showing 0 credentials')
                ->assertDontSee('.card-title', 'DeletCredential')
                ->assertDontSee('https://credential.com/delete')
                ->assertDontSee('delete')
                ->assertDontSee('**********');
        });
    }
}
