<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Credential;
use App\Http\Requests\CredentialRequest;
use Illuminate\Http\JsonResponse;

class CredentialsController
{
    public function index(): JsonResponse
    {
        $credentials = auth()->user()->credentials;

        return response()->json(compact('credentials'));
    }

    public function store(CredentialRequest $request): JsonResponse
    {
        $request->merge([
            'password' => encrypt($request->get('password')),
        ]);

        $credential = auth()->user()->credentials()->create($request->all());

        return response()->json(compact('credential'));
    }

    public function update(CredentialRequest $request, Credential $credential): JsonResponse
    {
        $request->merge([
            'password' => encrypt($request->get('password')),
        ]);

        $credential->update($request->all());

        return response()->json(compact('credential'));
    }

    public function destroy(Credential $credential): JsonResponse
    {
        $credential->delete();

        return response()->json(compact('credential'));
    }
}
