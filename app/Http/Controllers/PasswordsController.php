<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Credential;

class PasswordsController extends Controller
{
    public function show(Credential $credential)
    {
        return response()->json([
            'password' => decrypt($credential->password)
        ]);
    }
}
